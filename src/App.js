import React from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import ListProjectComponent from "./component/project/ListProjectComponent.js"
import AddProjectComponent from "./component/project/AddProjectComponent.js";
import EditProjectComponent from "./component/project/EditProjectComponent.js";

function App() {

  return (
      <div className="container">
        <Router>
          <div className="col-md-6">
            <h1 className="text-center" style={style}>React User Application</h1>
            <Switch>
              <Route path="/" exact component={ListProjectComponent} />
              <Route path="/projects" component={ListProjectComponent} />
              <Route path="/add-project" component={AddProjectComponent} />
              <Route path="/edit-project" component={EditProjectComponent} />
            </Switch>
          </div>
        </Router>
      </div>
  );
}

const style = {
    color: 'red',
    margin: '10px',
    display: 'center',
    align: 'center'
};

export default App;
