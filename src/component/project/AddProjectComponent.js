import React from "react";
import {Component} from "react";
import ApiService from "../../service/ApiService.js"

class AddProjectComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            name: "",
            description: "",
            message: null
        }

        this.saveProject = this.saveProject.bind(this);
    }

    saveProject = (e) => {
        e.preventDefault();
        let project = {name: this.state.name, description: this.state.description};
        ApiService.addProject(project)
            .then(res => {
                this.setState({message : 'Project added successfully.'});
                this.props.history.push('/projects');
            });
    }

    onChange = (e) =>
        this.setState({ [e.target.name]: e.target.value });

    render() {
        return(
            <div>
                <h2 className="text-center">Add Project</h2>
                <form>
                    <div className="form-group">
                        <label>Project Name:</label>
                        <input type="text" placeholder="name" name="name" className="form-control" value={this.state.name} onChange={this.onChange}/>
                    </div>

                    <div className="form-group">
                        <label>Project Description:</label>
                        <input type="text" placeholder="description" name="description" className="form-control" value={this.state.password} onChange={this.onChange}/>
                    </div>

                    <button className="btn btn-success" onClick={this.saveProject}>Save</button>
                </form>
            </div>
        );
    }
}

export default AddProjectComponent;