import React from "react";
import {Component} from "react";
import ApiService from "../../service/ApiService.js"
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button';
import CreateIcon from '@material-ui/icons/Create';
import DeleteIcon from '@material-ui/icons/Delete';
import Typography from '@material-ui/core/Typography';

class ListProjectComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            projects: [],
            message: null
        };
        this.deleteProject = this.deleteProject.bind(this);
        this.editProject = this.editProject.bind(this);
        this.addProject = this.addProject.bind(this);
        this.reloadProjectList = this.reloadProjectList.bind(this);
    }

    componentDidMount() {
        this.reloadProjectList();
    }

    reloadProjectList() {
        ApiService.fetchProjects()
            .then((res) => {
                this.setState({projects: res.data});
            });
    }

    deleteProject(projectId) {
        ApiService.deleteProject(projectId)
            .then(res => {
                this.setState({message: "Project deleted successfully"});
                this.setState({projects : this.state.projects.filter(project => project.id !== projectId)});
            })
    }

    editProject(id) {
        window.localStorage.setItem("projectId", id);
        this.props.history.push('/edit-project');
    }

    addProject() {
        window.localStorage.removeItem("projectId");
        this.props.history.push('/add-project');
    }

    render() {
        return (
            <div>
                <Typography variant="h4" style={style}>Project Details</Typography>
                <Button variant="contained" color="primary" onClick={() => this.addProject()}> Add Project</Button>
                <Table>
                    <TableHead>
                    <TableRow>
                        <TableCell className="hidden">Id</TableCell>
                        <TableCell>Name</TableCell>
                        <TableCell>Description</TableCell>
                    </TableRow>
                    </TableHead>
                    <TableBody>
                    {
                        this.state.projects.map(
                            project =>
                                <TableRow key={project.id}>
                                    <TableCell component="th" scope="row">
                                        {project.id}
                                    </TableCell>
                                    <TableCell align="center">{project.name}</TableCell>
                                    <TableCell align="center">{project.description}</TableCell>
                                    <TableCell align="center" onClick={() => this.editProject(project.id)}>
                                        <CreateIcon/>
                                    </TableCell>
                                    <TableCell align="right">
                                        <TableCell align="right" onClick={() => this.deleteProject(project.id)}>
                                        <DeleteIcon/>
                                    </TableCell>
                                    </TableCell>
                                </TableRow>
                        )
                    }
                    </TableBody>
                </Table>

            </div>
        );
    }
}

const style ={
    display: 'flex',
    justifyContent: 'center'
}

export default ListProjectComponent;