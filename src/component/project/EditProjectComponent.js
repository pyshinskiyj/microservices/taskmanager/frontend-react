import React, {Component} from "react";
import ApiService from "../../service/ApiService.js"

class EditProjectComponent extends Component {

    constructor(props) {
        super(props);
        this.state ={
            id: '',
            name: '',
            description: ''
        };
        this.saveProject = this.saveProject.bind(this);
        this.loadProject = this.loadProject.bind(this);
    }

    componentDidMount() {
        this.loadProject();
    }

    loadProject() {
        ApiService.fetchProjectById(window.localStorage.getItem("projectId"))
            .then((res) => {
                let project = res.data;

                this.setState({
                    id: project.id,
                    name: project.name,
                    description: project.description
                })
            });
    }

    onChange = (e) =>
        this.setState({ [e.target.name]: e.target.value });

    saveProject = (e) => {
        e.preventDefault();
        let project = {id: this.state.id, name: this.state.name, description: this.state.description};
        ApiService.editProject(project)
            .then(res => {
                this.setState({message : 'Project added successfully.'});
                this.props.history.push('/projects');
            });
    };

    render() {
        return (
            <div>
                <h2 className="text-center">Edit Project</h2>
                <form>

                    <div className="form-group">
                        <label>Project Name:</label>
                        <input type="text" placeholder="name" name="name" className="form-control" readOnly="true" defaultValue={this.state.name}/>
                    </div>

                    <div className="form-group">
                        <label>Project Description:</label>
                        <input placeholder="description" name="description" className="form-control" value={this.state.description} onChange={this.onChange}/>
                    </div>

                    <button className="btn btn-success" onClick={this.saveProject}>Save</button>
                </form>
            </div>
        );
    }
}

export default EditProjectComponent;