import axios from 'axios';

const PROJECT_API_BASE_URL = "http://localhost:8080/api/project/";

class ApiService {

    fetchProjects() {
        return axios.get(PROJECT_API_BASE_URL + 'all');
    }

    fetchProjectById(projectId) {
        return axios.get(PROJECT_API_BASE_URL + projectId);
    }

    deleteProject(projectId) {
        return axios.delete(PROJECT_API_BASE_URL + 'delete/' + projectId);
    }

    addProject(project) {
        return axios.post(PROJECT_API_BASE_URL + 'create/', project);
    }

    editProject(project) {
        return axios.put(PROJECT_API_BASE_URL + 'update/' + project.id, project);
    }
}

export default new ApiService();